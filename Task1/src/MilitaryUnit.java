public abstract class MilitaryUnit {
    protected int health=0;
    protected float resistance=0;
    protected int crew=0;
    protected boolean alive=true;

    protected MilitaryUnit(int health, float resistance, int crew, boolean alive){
        this.health = health;
        this.resistance = resistance;
        this.crew = crew;
        this.alive = alive;
    }

    protected void setHealth(int health){
        this.health = health;
    }

    public int getHealth(){
        return this.health;
    }

    protected void setResistance(int resistance){
        this.resistance = resistance;
    }

    public float getResistance(){
        return this.resistance;
    }

    public void setCrew(int crew){
        this.crew = crew;
    }

    public int getCrew(){
        return this.crew;
    }

    public abstract int damage(int damage);
    public abstract void destroy();
}
