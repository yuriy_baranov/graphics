public interface CrewCommands {
    int addTroops(int n_troops);
    int removeTroops(int n_troops);
}
