public class Ifv3 extends Ifv {

    public Ifv3(int troops) {
        super(1000, 0.5f, 2, 7, troops);
    }

    @Override
    public void shot() {
        System.out.println("Tra-ta-ta-ta-ta-ta-ta!");
    }
}
