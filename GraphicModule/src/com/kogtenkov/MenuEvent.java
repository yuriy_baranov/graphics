package com.kogtenkov;

public class MenuEvent {
    public String name;
    public int[] intdata;
    public float[] floatdata;
    public String[] strdata;

    public MenuEvent(String name, int[] intdata, float[] floatdata, String[] strdata) {
        this.name = name;
        this.floatdata = floatdata;
        this.intdata = intdata;
        this.strdata = strdata;
    }
}
