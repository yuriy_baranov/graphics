package com.kogtenkov;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.glu.GLU;
import java.util.Date;

import static java.lang.Math.*;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class FollowingCamera extends Camera {
    private double radius;
    private double min_radius = 1;
    private double max_radius = 100;
    private Followable object;

    public FollowingCamera(GLWindow glwindow, InputModule input_module, String name, double speed,
                           double hrot, double vrot, double radius, Followable object, boolean[] keys, DeltaTimeCalculator deltaTimeCalculator) {
        this.name = name;
        this.hrot = hrot;
        this.vrot = vrot;
        this.keys = keys;
        this.glwindow = glwindow;
        this.input_module = input_module;
        this.radius = radius;
        this.object = object;
        this.speed = speed;

        this.deltaTimeCalculator = deltaTimeCalculator;
    }

    public void work(GLU glu) {
        double delta_time = deltaTimeCalculator.getDelta();
        radius -= speed * input_module.mouse_rotation * sqrt(radius);
        radius = max(radius, min_radius);
        radius = min(radius, max_radius);
        input_module.mouse_rotation = 0;
        x = cos(hrot) * cos(vrot) * radius + object.getX();
        y = sin(vrot) * radius + object.getY();
        z = sin(hrot) * cos(vrot) * radius + object.getZ();
        if (keys[2]) {
            glwindow.setPointerVisible(false);
            glwindow.confinePointer(true);
            int mouse_dx = input_module.cur_mouse_x - input_module.prev_mouse_x;
            int mouse_dy = input_module.cur_mouse_y - input_module.prev_mouse_y;
            hrot += mouse_dx * mouseRotSpeed * delta_time;
            vrot += mouse_dy * mouseRotSpeed * delta_time;
            hrot %= (2 * PI);
            if (vrot <= -PI / 2 + 0.005) vrot = -PI / 2 + 0.005;
            else if (vrot >= PI / 2 - 0.005) vrot = PI / 2 - 0.005;
            x = cos(hrot) * cos(vrot) * radius + object.getX();
            y = sin(vrot) * radius + object.getY();
            z = sin(hrot) * cos(vrot) * radius + object.getZ();
            glwindow.warpPointer(input_module.prev_mouse_x, input_module.prev_mouse_y);
            input_module.cur_mouse_x = input_module.prev_mouse_x;
            input_module.cur_mouse_y = input_module.prev_mouse_y;
        }
        else {
            glwindow.setPointerVisible(true);
            glwindow.confinePointer(false);
        }
        glu.gluLookAt(x, y, z, object.getX(), object.getY(), object.getZ(), 0, 1, 0);
    }

    public String getType() {
        return "Following";
    }
}
