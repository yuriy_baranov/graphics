package com.kogtenkov;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.glu.GLU;

import static java.lang.Math.PI;

public abstract class Camera {
    double x = 0, y = 0, z = 0;
    double hrot = PI / 2, vrot = 0;
    boolean[] keys;
    double speed = 7;
    double mouseRotSpeed = 0.2;         //rad per pixel
    DeltaTimeCalculator deltaTimeCalculator;
    GLWindow glwindow;
    InputModule input_module;
    String name;

    public void setPosition(double x, double y, double z, String name) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.name = name;
    }

    public double getX() {return x;}

    public double getY() {return y;}

    public double getZ() {return z;}

    public String getName() {
        return name;
    }

    public void setMouseRotSpeed(double mouse_rot_speed) {
        this.mouseRotSpeed = mouse_rot_speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void work(GLU glu) { }

    abstract public String getType();
}
