package com.kogtenkov;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;

public class InputModule implements KeyListener, MouseListener {
    public boolean[] keys;
    public int cur_mouse_x = 0;
    public int cur_mouse_y = 0;
    public int prev_mouse_x = 0;
    public int prev_mouse_y = 0;
    public float mouse_rotation = 0;
    private int mouse_dragged = -1;

    InputModule(boolean[] keys) {
        this.keys = keys;
    }


    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isAutoRepeat()) {
            keys[keyEvent.getKeyCode()] = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (!keyEvent.isAutoRepeat()) {
            keys[keyEvent.getKeyCode()] = false;
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        keys[mouseEvent.getButton() - 1] = false;
        mouse_dragged = -1;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        keys[mouseEvent.getButton() - 1] = true;
        cur_mouse_x = mouseEvent.getX();
        cur_mouse_y = mouseEvent.getY();
        prev_mouse_x = mouseEvent.getX();
        prev_mouse_y = mouseEvent.getY();
        mouse_dragged = -1;
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        keys[mouseEvent.getButton() - 1] = false;
        mouse_dragged = -1;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        cur_mouse_x = mouseEvent.getX();
        cur_mouse_y = mouseEvent.getY();
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (mouse_dragged > 2) {
            cur_mouse_x = mouseEvent.getX();
            cur_mouse_y = mouseEvent.getY();
        }
        mouse_dragged += 1;
    }

    @Override
    public void mouseWheelMoved(MouseEvent mouseEvent) {
        mouse_rotation += mouseEvent.getRotation()[1];
    }
}
