package com.kogtenkov;

import com.jogamp.opengl.GL2;

import java.util.ArrayList;

public class ScreenMenu {

    private ArrayList<MenuButton> buttons = new ArrayList<>();
    private ArrayList<TextPanel> text_panels = new ArrayList<>();
    private ArrayList<ButtonList> button_lists = new ArrayList<>();
    private boolean is_active = true;
    private boolean is_visible = true;
    private GL2 gl;
    private boolean[] keys;
    private InputModule input_module;

    ScreenMenu(GL2 gl, boolean[] keys, InputModule input_module) {
        this.gl = gl;
        this.keys = keys;
        this.input_module = input_module;
    }

    public void set_active(boolean is_active) {
        this.is_active = is_active;
    }

    public void set_visible(boolean is_visible) {
        this.is_visible = is_visible;
    }

    public void draw(int width, int height) {
        if (is_visible) {
            for (int i = 0; i < buttons.size(); i++) {
                buttons.get(i).draw(width, height);
            }
            for (int i = 0; i < text_panels.size(); i++) {
                text_panels.get(i).draw(width, height);
            }
            for (int i = 0; i < button_lists.size(); i++) {
                button_lists.get(i).draw(width, height);
            }
        }
    }

    public void addButton(int x, int y, int size_x, int size_y, String title, String event_name, int text_x, int text_y, int font_size) {
        buttons.add(new MenuButton(x, y, size_x, size_y, title, gl, keys, event_name, text_x, text_y, font_size));
    }

    public void addTextPanel(int x, int y, int size_x, int size_y, String name, int text_x, int text_y, int font_size, int symb_in_str, int break_size) {
        text_panels.add(new TextPanel(x, y, size_x, size_y, name, gl, text_x, text_y, font_size, symb_in_str, break_size));
    }

    public void addButtonList(int x, int y, int size_x, int size_y, int button_size_x, int button_size_y, int vspace, int hspace) {
        button_lists.add(new ButtonList(x, y, size_x, size_y, gl, button_size_x, button_size_y, vspace, hspace, keys, input_module));
    }

    public void addButtonList(ButtonList buttonList) {
        button_lists.add(buttonList);
    }

    public void addListener(MenuListener menuListener, String event_name) {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).get_event_name().equals(event_name)) {
                buttons.get(i).addListener(menuListener);
            }
        }
    }

    public int rewriteTextPanel(String name, String text) {
        int count = 0;
        for (int i = 0; i < text_panels.size(); i++) {
            if (text_panels.get(i).getName().equals(name)) {
                text_panels.get(i).rewrite(text);
                count++;
            }
        }
        return count;
    }

    public void work(int width, int height) {
        if (is_active) {
            for (int i = 0; i < button_lists.size(); i++) {
                button_lists.get(i).work(width, height);
            }
            for (int i = 0; i < buttons.size(); i++) {
                boolean check_status = buttons.get(i).check_click(input_module.cur_mouse_x, input_module.cur_mouse_y, width, height);
                if (check_status) {
                    keys[0] = false;
                    return;
                }
            }
        }
    }
}
