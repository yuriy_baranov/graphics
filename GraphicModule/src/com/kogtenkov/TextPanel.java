package com.kogtenkov;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.awt.TextRenderer;

import java.awt.*;

public class TextPanel {
    private int x;
    private int y;
    private int size_x;
    private int size_y;
    boolean is_visible = true;
    private String name;
    private GL2 gl;
    private TextRenderer textRenderer;
    private int text_x, text_y;
    private int x1, x2, y1, y2;
    private String text = "";
    private int symb_in_str;
    private int break_size;

    public TextPanel(int x, int y, int size_x, int size_y, String name, GL2 gl, int text_x, int text_y, int font_size, int symb_in_str, int break_size) {
        this.x = x;
        this.y = y;
        this.size_x = size_x;
        this.size_y = size_y;
        this.name = name;
        this.gl = gl;
        this.text_x = text_x;
        this.text_y = text_y;
        this.textRenderer = new TextRenderer(new Font("Arial", Font.PLAIN, font_size));
        this.symb_in_str = symb_in_str;
        this.break_size = break_size;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public void rewrite(String new_text) {
        this.text = new_text;
    }

    public void set_visible(boolean is_visible) {
        this.is_visible = is_visible;
    }

    private String line_break(String text) {
        String[] lines = text.split("\n");
        String ans = "";
        for (int j = 0; j < lines.length; j++) {
            String[] substr = lines[j].split(" ");
            int cur_strlen = 0;
            for (int i = 0; i < substr.length; i++) {
                if (cur_strlen == 0 || substr[i].length() + 1 + cur_strlen < this.symb_in_str) {
                    if (cur_strlen > 0) {
                        cur_strlen++;
                        ans += " ";
                    }
                    cur_strlen += substr[i].length();
                    ans += substr[i];
                } else {
                    ans += "\n";
                    ans += substr[i];
                    cur_strlen = substr[i].length();
                }
            }
            ans += "\n";
        }
        return ans;
    }

    public void draw(int width, int height) {
        if (this.is_visible) {
            gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
            gl.glColor3f(0.4f, 0.4f, 0.4f);
            if (x >= 0) {
                x1 = x;
                x2 = x + size_x;
            } else {
                x1 = width + x;
                x2 = x1 + size_x;
            }
            if (y >= 0) {
                y1 = y;
                y2 = y + size_y;
            } else {
                y1 = height + y;
                y2 = y1 + size_y;
            }
            gl.glBegin(gl.GL_QUAD_STRIP);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y1);
            gl.glVertex2f(x1, y2);
            gl.glVertex2f(x2, y2);
            gl.glEnd();
            gl.glColor3f(1, 1, 1);
            textRenderer.beginRendering(width, height);
            String[] form_text = line_break(text).split("\n");
            for (int i = 0; i < form_text.length; i++) {
                textRenderer.draw(form_text[i], x1 + text_x, y1 + text_y - break_size * i);
            }
            textRenderer.endRendering();
        }
    }


}
