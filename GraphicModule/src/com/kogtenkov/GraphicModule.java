package com.kogtenkov;

import com.jogamp.newt.event.*;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import com.jogamp.opengl.glu.GLU;

import java.awt.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

import com.jogamp.opengl.util.FPSAnimator;
import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

public class GraphicModule implements GLEventListener, MenuListener, Module {

    public static DisplayMode dm, dm_old;
    private FPSAnimator animator;
    private GLU glu = new GLU();
    private boolean[] keys;
    private InputModule input_module;
    private CameraDispatcher cameraDispatcher;
    private World world;
    private GLWindow glwindow;
    private Quadcopter quadcopter;
    private ScreenMenu screenMenu;
    private long prev_time = 0;
    private DeltaTimeCalculator deltaTimeCalculator;
    private Context api;
    private ShaderProgram shaderProgram;
    private Path data;
    private Logger logger;

    private void Begin2D(GL2 gl, int width, int height) {
        gl.glDisable(GL_DEPTH_TEST);
        gl.glMatrixMode(GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        glu.gluOrtho2D(0, width, 0, height);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
    }

    private void End2D(GL2 gl) {
        gl.glPopMatrix();
        gl.glMatrixMode(GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glEnable ( GL_DEPTH_TEST );
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        GL2 gl = drawable.getGL().getGL2();
        int width = drawable.getSurfaceWidth();
        int height = drawable.getSurfaceHeight();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity(); // Reset The View

        deltaTimeCalculator.update();
        if (deltaTimeCalculator.getCurTime() - prev_time >= 1000) {
            int frames_per_sec = (int)(1.0 / deltaTimeCalculator.getDelta());
            screenMenu.rewriteTextPanel("FPS panel", "FPS: " + String.valueOf(frames_per_sec));
            prev_time = deltaTimeCalculator.getCurTime();
        }

        quadcopter.update();

        screenMenu.work(width, height);

        cameraDispatcher.work(glu);

        gl.glUseProgram(shaderProgram.getProgramId());

        gl.glEnable(GL2.GL_LIGHTING);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[] {0.2f, 0.2f, 0.2f, 0.0f}, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[] {1.0f, 1.0f, 0.9f, 0.0f}, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float[] {1.0f, 1.0f, 1.0f, 0.0f}, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[] {0.8f, 0.2f, 0.0f, 0.0f}, 0);

        quadcopter.draw();
        world.Draw();

        gl.glUseProgram(0);

        gl.glDisable(GL2.GL_LIGHTING);

        gl.glFlush();

        Begin2D(gl, width, height);
        screenMenu.draw(width, height);
        End2D(gl);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        // method body
    }

    private void loadShader(GL2 gl) {
        File input_file = data.resolve("shader_config.txt").toFile();
        Scanner scanner;
        try {
            scanner = new Scanner(input_file).useLocale(Locale.US);
        } catch (Exception e) {
            System.out.println("Cannot read shader config");
            System.exit(-1);
            return;
        }

        String vs_shader_name = scanner.nextLine().trim();
        String fs_shader_name = scanner.nextLine().trim();
        scanner.close();
        Path shaders = this.data.resolve("shaders");
        File vertexShader = shaders.resolve(vs_shader_name).toFile();
        File fragmentShader = shaders.resolve(fs_shader_name).toFile();
        logger.info(String.format("Shaders: %s %s", vertexShader.toString(), fragmentShader.toString()));
        shaderProgram = new ShaderProgram();
        if (!shaderProgram.init(gl, vertexShader, fragmentShader)) {
            throw new IllegalStateException("Unable to initiate the shaders!");
        }
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        final GL2 gl = drawable.getGL().getGL2();
        gl.glShadeModel(GL2.GL_SMOOTH);
        gl.glClearColor(135f / 256f, 206f / 256f, 235f / 256f, 0);
        gl.glClearDepth(1.0f);
        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glDepthFunc(GL2.GL_LEQUAL);
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);
        //
        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_NORMALIZE);
        gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT );
        gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT );

        gl.glEnable(GL2.GL_LIGHT1);

        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, new float[] {0.1f, 0.1f, 0.1f, 0.0f}, 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SHININESS, new float[] {0.1f}, 0);

        keys = new boolean[256];
        input_module = new InputModule(keys);
        deltaTimeCalculator = new DeltaTimeCalculator(15);
        File worldModelPath = this.data.resolve("models").resolve("world3.obj").toFile();
        File texturePath = this.data.resolve("textures").resolve("005.jpg").toFile();
        File quadInfoPath = this.data.resolve("quadcopter_info.txt").toFile();
        world = new World(worldModelPath, texturePath, gl);
        quadcopter = new Quadcopter(this.data, quadInfoPath, gl, deltaTimeCalculator, api);
        cameraDispatcher = new CameraDispatcher(glwindow, input_module, keys, deltaTimeCalculator);
        cameraDispatcher.addCamera(new FollowingCamera(glwindow, input_module, "Quadcopter camera", 0.5, 0, 0, 3, quadcopter, keys, deltaTimeCalculator));
        FreeCamera tmp_camera = new FreeCamera(glwindow, input_module, 6, 10, 8, "Free camera", 0, 0, keys, deltaTimeCalculator);
        tmp_camera.attach(quadcopter);
        cameraDispatcher.addCamera(tmp_camera);
        screenMenu = new ScreenMenu(gl, keys, input_module);
        screenMenu.addButton(15, 15, 175, 50, "Next camera", "change_camera", 10, 15, 28);
        screenMenu.addButton(15, -65, 175, 50, "Exit", "exit", 65, 15, 28);
        //screenMenu.addButton(210, 15, 150, 50, "Show list", "show_list", 10, 15, 28);
        //screenMenu.addButton(370, 15, 150, 50, "Hide list", "hide_list", 10, 15, 28);
        screenMenu.addListener(cameraDispatcher, "change_camera");
        screenMenu.addListener(this, "exit");
        screenMenu.addTextPanel(-85, -35, 85, 35, "FPS panel", 5, 10, 20, 15, 40);
        //screenMenu.addTextPanel(-300, 10, 290, 200, "Hello there panel", 5, 170, 22, 25, 30);
        //screenMenu.rewriteTextPanel("Hello there panel", "Sometimes you just need a little less shader...\n(c)\nAlexey Kogtenkov");
        /*ButtonList button_list = new ButtonList(550, 15, 120, 200, gl, 100, 40, 7, 10, keys, input_module);
        button_list.addButton("Button №1", "EVENT_NAME", 5, 10, 18);
        button_list.addButton("Button №2", "EVENT_NAME", 5, 10, 18);
        button_list.addButton("Button №3", "EVENT_NAME", 5, 10, 18);
        button_list.addButton("Button №4", "EVENT_NAME", 5, 10, 18);
        button_list.addButton("Button №5", "EVENT_NAME", 5, 10, 18);
        button_list.addButton("Button №6", "EVENT_NAME", 5, 10, 18);
        button_list.set_visible(false);
        button_list.set_active(false);
        screenMenu.addListener(button_list, "show_list");
        screenMenu.addListener(button_list, "hide_list");
        screenMenu.addButtonList(button_list);*/

        loadShader(gl);

        deltaTimeCalculator.reset(15);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        // TODO Auto-generated method stub
        final GL2 gl = drawable.getGL().getGL2();
        if(height <= 0)
            height = 1;

        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        glu.gluPerspective(45.0f, h, 0.01, 200.0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public GraphicModule()
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void menuEventReceiver(MenuEvent e) {
        this.animator.stop();
        api.exit();
        System.exit(0);
    }

    @Override
    public void init(JsonValue config, Context api) throws Exception {
        JsonValue data = config.get("dataDir");
        if (!data.isString()) {
            throw new BplaException("Expected string \"dataDir\" in parameters");
        }
        this.data = new File(data.asString()).toPath();
        this.logger = api.getLogger();
    }

    @Override
    public void run(Context api) {
        this.api = api;

        final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);

        this.glwindow = GLWindow.create(capabilities);
        this.animator = new FPSAnimator(this.glwindow, 200, true);

        glwindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDestroyNotify(WindowEvent arg0) {
                // Use a dedicate thread to run the stop() to ensure that the
                // animator stops before program exits.
                new Thread() {
                    @Override
                    public void run() {
                        if (animator.isStarted())
                            animator.stop();    // stop the animator loop
                        api.exit();
                        System.exit(0);
                    }
                }.start();
            }
        });
        this.glwindow.addGLEventListener(this);

        this.glwindow.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) { input_module.keyPressed(e); }
            public void keyReleased(KeyEvent e) {
                input_module.keyReleased(e);
            }
        });

        this.glwindow.requestFocus();
        this.glwindow.setSize(600, 600);
        this.glwindow.setTitle("Quadcopter Simulator");
        this.glwindow.setVisible(true);

        this.glwindow.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                input_module.mousePressed(e);
            }
            public void mouseReleased(MouseEvent e) {
                input_module.mouseReleased(e);
            }
            public void mouseClicked(MouseEvent e) {
                input_module.mouseClicked(e);
            }
            public void mouseDragged(MouseEvent e) { input_module.mouseDragged(e); }
            public void mouseWheelMoved(MouseEvent e) {
                input_module.mouseWheelMoved(e);
            }
            public void mouseMoved(MouseEvent e) {
                input_module.mouseMoved(e);
            }
        });

        animator.start();
    }

    /*public static void main(String[] args) {

    }*/
}