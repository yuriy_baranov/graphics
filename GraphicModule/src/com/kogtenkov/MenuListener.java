package com.kogtenkov;

public interface MenuListener {
    void menuEventReceiver(MenuEvent e);
}
