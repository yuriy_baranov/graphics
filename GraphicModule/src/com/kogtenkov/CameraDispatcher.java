package com.kogtenkov;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.glu.GLU;

import java.util.ArrayList;

public class CameraDispatcher implements MenuListener {
    private ArrayList<Camera> cameras;
    private int cur_camera = -1;
    private boolean[] keys;
    private DeltaTimeCalculator deltaTimeCalculator;

    public CameraDispatcher(GLWindow glwindow, InputModule input_module, boolean[] keys, DeltaTimeCalculator deltaTimeCalculator) {
        this.keys = keys;
        this.deltaTimeCalculator = deltaTimeCalculator;
        cameras = new ArrayList<>();
        cameras.add(new FreeCamera(glwindow, input_module, 0.0, 3.0, 0.0, "Default camera", 0.5, 0.0, keys, deltaTimeCalculator));
        cur_camera = 0;
    }

    public void addCamera(Camera camera) {
        cameras.add(camera);
    }

    public void deleteCamera(int id) {
        if (cameras.size() > id && id > 0) {
            cameras.remove(id);
            if (cameras.size() <= cur_camera) {
                cur_camera = cameras.size() - 1;
            }
        }
    }

    public void work(GLU glu) {
        if (keys[9]) {          //TAB
            cur_camera = (cur_camera + 1) % cameras.size();
            keys[9] = false;
        }
        cameras.get(cur_camera).work(glu);
    }

    public String[] getCamerasNames() {
        String[] names = new String[cameras.size()];
        for (int i = 0; i < names.length; i++) {
            names[i] = cameras.get(i).getName();
        }
        return names;
    }

    @Override
    public void menuEventReceiver(MenuEvent e) {
        if (e.name.equals("change_camera")) {
            cur_camera = (cur_camera + 1) % cameras.size();
        }
    }
}
