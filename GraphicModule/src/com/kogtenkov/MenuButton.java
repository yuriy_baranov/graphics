package com.kogtenkov;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.awt.TextRenderer;

import java.awt.*;
import java.util.ArrayList;

public class MenuButton {
    private int x;
    private int y;
    private int size_x;
    private int size_y;
    boolean is_active = true;
    boolean is_visible = true;
    private String title;
    private String event_name;
    private GL2 gl;
    private ArrayList<MenuListener> listeners;
    private boolean[] keys;
    private TextRenderer textRenderer;
    private int text_x, text_y;
    private int x1, x2, y1, y2;

    public MenuButton(int x, int y, int size_x, int size_y, String title, GL2 gl, boolean[] keys, String event_name, int text_x, int text_y, int font_size) {
        this.x = x;
        this.y = y;
        this.size_x = size_x;
        this.size_y = size_y;
        this.title = title;
        this.gl = gl;
        listeners = new ArrayList<>();
        this.keys = keys;
        this.event_name = event_name;
        this.textRenderer = new TextRenderer(new Font("Arial", Font.PLAIN, font_size));
        this.text_x = text_x;
        this.text_y = text_y;
    }

    public void addListener(MenuListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MenuListener listener) {
        listeners.remove(listener);
    }

    public void set_visible(boolean is_visible) {
        this.is_visible = is_visible;
    }

    public void set_active(boolean is_active) {
        this.is_active = is_active;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSizeX() {
        return size_x;
    }

    public int getSizeY() {
        return size_y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void draw(int width, int height) {
        if (is_visible) {
            gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
            gl.glColor3f(0.3f, 0.3f, 0.3f);
            gl.glBegin(gl.GL_QUAD_STRIP);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y1);
            gl.glVertex2f(x1, y2);
            gl.glVertex2f(x2, y2);
            gl.glEnd();
            gl.glColor3f(1, 1, 1);
            textRenderer.beginRendering(width, height);
            textRenderer.draw(title, x1 + text_x, y1 + text_y);
            textRenderer.endRendering();
        }
    }

    public boolean isCursorOnButton(int mouse_x, int mouse_y) {
        return (mouse_x >= x1 && mouse_x <= x2 && mouse_y >= y1 && mouse_y <= y2);
    }

    public boolean check_click(int mouse_x, int mouse_y, int width, int height) {
        if (is_active) {
            if (x >= 0) {
                x1 = x;
                x2 = x + size_x;
            } else {
                x1 = width + x;
                x2 = x1 + size_x;
            }
            if (y >= 0) {
                y1 = y;
                y2 = y + size_y;
            } else {
                y1 = height + y;
                y2 = y1 + size_y;
            }
            if (keys[0] && !keys[1] && !keys[2] && isCursorOnButton(mouse_x, height - mouse_y)) {
                for (int i = 0; i < listeners.size(); i++) {
                    listeners.get(i).menuEventReceiver(new MenuEvent(event_name, new int[0], new float[0], new String[0]));
                }
                return true;
            }
        }
        return false;
    }

    public String get_event_name() {
        return event_name;
    }

    public String getTitle() {
        return title;
    }
}
