import java.util.concurrent.Semaphore;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
        int max_containers = 10;
        Semaphore semaphore=new Semaphore(max_containers);
        ArrayList<Container> containers = new ArrayList<>();

        Thread thread_adder = new Thread(new ThreadAdder(containers, semaphore));
        Thread thread_deleter = new Thread(new ThreadDeleter(containers, semaphore));
        ThreadGetter thread_getter = new ThreadGetter(containers, semaphore);

        thread_adder.start();
        thread_getter.start();
        thread_deleter.start();
    }
}

