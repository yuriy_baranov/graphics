import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class ThreadGetter extends Thread {
    ArrayList<Container> container_list;
    Semaphore semaphore;

    public ThreadGetter(ArrayList<Container> container_list, Semaphore semaphore) {
        this.container_list = container_list;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
                return;
            }
            synchronized (container_list) {
                if (container_list.size() > 0) {
                    System.out.println("GET[0]: " + container_list.get(0).getName());
                    semaphore.release(1);
                }
            }
        }
    }
}
