import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class ThreadDeleter implements Runnable {
    ArrayList<Container> container_list;
    Semaphore semaphore;

    public ThreadDeleter(ArrayList<Container> container_list, Semaphore semaphore) {
        this.container_list = container_list;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
                return;
            }
            String name;
            synchronized (container_list) {
                name = container_list.get(0).getName();
                container_list.remove(0);
            }
            System.out.println("DELETED " + name);
            semaphore.release(1);
        }
    }
}

